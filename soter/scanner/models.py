"""
Module containing models that scanners should use for communication with the main API.
"""

import datetime
from enum import Enum
from functools import total_ordering
import re
from typing import Any, Dict, Optional, Set

from pydantic import BaseModel, HttpUrl, validator, constr
from pydantic.dataclasses import dataclass


class ScannerStatus(BaseModel):
    """
    Model for the status of a scanner.
    """
    #: The kind of the scanner
    kind: constr(min_length = 1)
    #: The vendor of the scanner
    vendor: constr(min_length = 1)
    #: The scanner version
    version: constr(min_length = 1)
    #: Indicates whether the scanner is available
    available: bool
    #: Message associated with the status
    message: Optional[constr(min_length = 1)] = None
    #: Additional properties associated with the scanner, e.g. time of last database update
    properties: Optional[Dict[str, Any]] = None


@total_ordering
class Severity(Enum):
    """
    Enum of possible severity levels for an issue.
    """
    NEGLIGIBLE = "NEGLIGIBLE"
    LOW = "LOW"
    MEDIUM = "MEDIUM"
    HIGH = "HIGH"
    UNKNOWN = "UNKNOWN"
    CRITICAL = "CRITICAL"

    def __lt__(self, other):
        # Assume that the severities are defined in order
        severities = list(self.__class__)
        return severities.index(self) < severities.index(other)


class Image(BaseModel):
    """
    Model for a Docker image.
    """
    #: The registry for the image
    registry: constr(min_length = 1)
    #: The repository for the image
    repository: constr(min_length = 1)
    #: The tag for the image. Can be ``None`` if there is no tag
    tag: Optional[constr(min_length = 1)]
    #: The digest for the image
    digest: constr(min_length = 1)
    #: The manifest for the image
    manifest: Dict[str, Any]
    #: The authorization header to use to fetch layers
    authorization: Optional[constr(min_length = 1)]
    #: The datetime that the image was created
    created: datetime.datetime

    @property
    def full_tag(self):
        tag = self.tag or 'unknown'
        return f'{self.registry}/{self.repository}:{tag}'

    @property
    def full_digest(self):
        return f'{self.registry}/{self.repository}@{self.digest}'

    def layer_uri(self, digest):
        return f"https://{self.registry}/v2/{self.repository}/blobs/{digest}"


class PackageType(Enum):
    """
    Enum of possible values for the package type of an image vulnerability.
    """
    #: The vulnerability is in a package installed using the OS package manager, e.g. yum, apt
    OS = "os"
    #: The vulnerability is in a package not managed by the OS package manager
    NON_OS = "non-os"


EPOCH_REGEX = re.compile(r'^\d+:')


def strip_epoch(v):
    """
    Strips the epoch from the given version, if present.

    Some scanners include the epoch and some don't. The only consistent thing to do is to strip it.
    It is unlikely that there will be the exact same version string in two epochs.
    """
    # If the given version number has an epoch, strip it
    if v and EPOCH_REGEX.match(v):
        return EPOCH_REGEX.sub('', v, count = 1)
    else:
        return v


class ImageVulnerability(BaseModel):
    """
    Model for a vulnerability in an image.
    """
    #: The title of the vulnerability, usually a CVE id
    title: constr(min_length = 1)
    #: The severity of the vulnerability
    severity: Severity
    #: A URL to visit for more information, if available
    info_url: Optional[HttpUrl] = None
    #: The package name that the vulnerability applies to
    package_name: constr(min_length = 1)
    #: The package version that the vulnerability applies to
    package_version: constr(min_length = 1)
    #: The type of package
    package_type: PackageType
    #: The location of the package
    package_location: Optional[constr(min_length = 1)] = None
    #: The version at which the vulnerability is fixed, if it exists
    fix_version: Optional[constr(min_length = 1)] = None

    package_version_strip_epoch = validator('package_version', allow_reuse = True)(strip_epoch)
    fix_version_strip_epoch = validator('fix_version', allow_reuse = True)(strip_epoch)

    @validator('package_location')
    def check_package_location(cls, v, values):
        package_type = values.get('package_type')
        if package_type:
            if package_type == PackageType.OS:
                assert v is None, 'should not be given for OS packages'
            else:
                assert v is not None, 'required for non-OS packages'
        return v


@dataclass(eq = True, frozen = True)
class Resource:
    """
    Model representing a resource.
    """
    #: The kind of the resource
    kind: constr(min_length = 1)
    #: The name of the resource
    name: constr(min_length = 1)
    #: The namespace of the resource
    namespace: constr(min_length = 1) = 'default'


class ConfigurationIssue(BaseModel):
    """
    Model for an issue with a Kubernetes configuration.
    """
    #: The title of the vulnerability
    title: constr(min_length = 1)
    #: The severity of the vulnerability
    severity: Severity
    #: The suggested remediation for the issue
    suggested_remediation: Optional[constr(min_length = 1)] = None
    #: The resources affected by the configuration issue
    affected_resources: Set[Resource]
