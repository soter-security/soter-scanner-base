"""
Module containing configuration utilities for Soter scanners.
"""

import os

from pydantic import BaseModel, BaseConfig, ConfigError
from pydantic.utils import deep_update

import toml


class ConfigModel(BaseModel):
    """
    Base class for scanner configurations.
    """
    class Config(BaseConfig):
        default_config_path = None
        load_file = toml.load
        env_prefix = ''

    __config__ = Config

    def __init__(self, _path = None, **init_kwargs):
        super().__init__(**deep_update(
            # Start with the configuration from the specified path
            self._load_file(_path),
            # Override with settings from the environment
            self._load_environ(),
            # Override with given kwargs
            init_kwargs
        ))

    def _load_file(self, path):
        # If a configuration file is specified, load from it
        path = path or self.__config__.default_config_path
        if path:
            try:
                with open(path) as fh:
                    return self.__config__.load_file(fh)
            except FileNotFoundError:
                return {}
        else:
            return {}

    def _load_environ(self):
        env_vars = {}
        # For each field, see if there is an environment variable for it
        for field in self.__fields__.values():
            env_var = (self.__config__.env_prefix + field.name).upper()
            try:
                env_value = os.environ[env_var]
            except KeyError:
                # No value available - move on to the next field
                continue
            # If the field is complex, attempt to load the given value as JSON
            if field.is_complex():
                env_value = self.__config__.json_loads(env_value)
            # Store the discovered value under the alias for the field
            env_vars[field.alias] = env_value
        return env_vars
